
/**
 * 
 * Diberikan sebuah function shoppingTime(memberId, money) yang menerima dua parameter berupa string dan number.
 * Parameter pertama merupakan memberId dan parameter ke-2 merupakan value uang yang dibawa oleh member tersebut.
 * 
 * Toko X sedang melakukan SALE untuk beberapa barang, yaitu:
 * Sepatu brand Stacattu seharga 1500000
 * Baju brand Zoro seharga 500000
 * Baju brand H&N seharga 250000
 * Sweater brand Uniklooh seharga 175000
 * Casing Handphone seharga 50000
 * 
 * Buatlah function yang akan mengembalikan sebuah object dimana object tersebut berisikan informasi memberId, money, listPurchased dan changeMoney.
 * 
 * Jika memberId kosong maka tampilkan "Mohon maaf, toko X hanya berlaku untuk member saja"
 * Jika uang yang dimiliki kurang dari 50000 maka tampilkan "Mohon maaf, uang tidak cukup"
 * 
 * Member yang berbelanja di toko X akan membeli barang yang paling mahal terlebih dahulu
 * dan akan membeli barang-barang yang sedang SALE masing-masing 1 jika uang yang dimilikinya masih cukup.
 * 
 * Contoh jika inputan memberId: '324193hDew2' dan money: 700000
 * 
 * output:
 * { memberId: '324193hDew2', money: 700000, listPurchased: [ 'Baju Zoro', 'Sweater Uniklooh' ], changeMoney: 25000 }
 * 
 */

function shoppingTime(memberId, money) {
   // code here
   var stacattu = 1500000;
   var zoro = 500000;
   var h_n = 250000;
   var uniqlo = 175000;
   var termurah = 50000;

   var arr = {}
   var dompet = money;
   var shoppingcart = [];
   if (memberId === '' || (memberId === undefined)) {
      return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } 
    else if (money < 50000) {
      return "Mohon maaf, uang tidak cukup";
    } 
    else {
      if(dompet >= stacattu){
         shoppingcart.push('Sepatu Stacattu')
         dompet -= stacattu;
      }
      else if(dompet >= zoro){
         shoppingcart.push('Baju brand Zoro')
         dompet -= zoro
      }
      else if(dompet >= h_n){
         shoppingcart.push('Bahu brand H&N')
         dompet -= h_n
      }
      else if(dompet >= uniqlo){
         shoppingcart.push('Sweater brand Uniklooh')
         dompet -= uniqlo
      }
      else if(dompet >= termurah){
         shoppingcart.push('Casing Handphone')
         dompet -= termurah
      }
   }

   arr.memberId = memberId;
   arr.money = money;
   arr.cart = shoppingcart;
   arr.kembalian = dompet;
   return arr
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

