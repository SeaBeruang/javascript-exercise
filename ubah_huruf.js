/**
 * 
 * Diberikan function ubahHuruf(kata) yang akan menerima satu parameter berupa string.
 * Function akan me-return sebuah kata baru dimana setiap huruf akan digantikan dengan huruf alfabet setelahnya.
 * Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.
 * 
 */


 function ubahHuruf(changeToNextLetter) {
    var berubah = changeToNextLetter.split('');
    for (var i = 0; i < berubah.length; i++) {

        switch(berubah[i]) {
          case ' ':
            break;
          case 'z':
            berubah[i] = 'a';
            break;
          case 'Z':
            berubah[i] = 'A';
            break;
          default:
            berubah[i] = String.fromCharCode(1 + berubah[i].charCodeAt(0));
        }

    }
    return berubah.join('');
}
  
// TEST CASES
console.log(ubahHuruf('wow')); // xpx
console.log(ubahHuruf('developer')); // efwfmpqfs
console.log(ubahHuruf('javascript')); // kbwbtdsjqu
console.log(ubahHuruf('keren')); // lfsfo
console.log(ubahHuruf('semangat')); // tfnbohbu
